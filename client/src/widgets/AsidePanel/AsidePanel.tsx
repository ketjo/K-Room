import './style.scss'
import { ReactElement } from 'react'
import { ContactList, ChatRoomList, UserSettings, Calls } from './elements'
import { SelectedContentElement } from 'common-types'
import { WidgetWrapper } from '../WidgetWrapper/WidgetWrapper'
import { useTypedSelector } from 'src/shared/lib'

export const AsidePanel = () => {
  const { selectedContentElement } = useTypedSelector((state) => state.persist.settings)
  const TabComponents: Record<Exclude<SelectedContentElement, 'admin-panel' | 'info'>, ReactElement> = {
    contacts: <ContactList />,
    'chat-list': <ChatRoomList />,
    calls: <Calls />,
    settings: <UserSettings />
  }
  return (
    <div className="aside-panel">
      <WidgetWrapper placement="aside">
        <div className="aside-panel__content">
          {selectedContentElement in TabComponents
            ? TabComponents[selectedContentElement as keyof typeof TabComponents]
            : ''}
        </div>
      </WidgetWrapper>
    </div>
  )
}
