export const clientConstants = {
  emojis: [
    {
      key: 'xd',
      glyph: '😆',
      reactions: true
    },
    {
      key: 'heart',
      glyph: '❤️',
      reactions: true
    },
    {
      key: 'flame',
      glyph: '🔥',
      reactions: true
    },
    {
      key: 'skull',
      glyph: '💀'
    },
    {
      key: 'mizaru',
      glyph: '🙈'
    },
    {
      key: 'dog-dirt',
      glyph: '💩',
      reactions: true
    },
    {
      key: 'cry',
      glyph: '😭',
      reactions: true
    },
    {
      key: 'fuc*',
      glyph: '🤬'
    },
    {
      key: 'glasses',
      glyph: '😎'
    },
    {
      key: 'think',
      glyph: '🤔',
      reactions: true
    },
    {
      key: 'demon',
      glyph: '😈'
    },
    {
      key: 'good',
      glyph: '👍',
      reactions: true
    },
    {
      key: 'thanks',
      glyph: '🙏',
      reactions: true
    },
    {
      key: 'muscle',
      glyph: '💪'
    },
    {
      key: 'vomitory',
      glyph: '🤮'
    },
    {
      key: 'alien',
      glyph: '👽'
    },
    {
      key: 'mask',
      glyph: '😷'
    },
    {
      key: 'brain blow',
      glyph: '🤯'
    }
  ],
  dimensions: {
    chatRoomHeaderHeight: 55,
    shortInputMessage: 35,
    fullInputMessage: 100,
    roomHeader: 55,
    contextMenuWidth: 150,
    contextMenuHeight: 150
  }
}
