import './style.scss'
export const CallDots = () => {
  return (
    <div className="lds-ellipsis">
      <div />
      <div />
      <div />
    </div>
  )
}
