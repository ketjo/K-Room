export type ModalContentComponentName =
  | 'user-data-settings-popup'
  | 'tech-settings-popup'
  | 'forward-message-popup'
  | 'create-multiple-chat-popup'
  | 'chat-room-settings-popup'
  | 'message-with-bind-data-popup'
