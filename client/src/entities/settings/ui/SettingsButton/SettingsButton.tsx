import { Button } from 'src/shared/ui'

export const SettingsButton = () => {
  return <Button type="radio" value="settings" iconName="settings-cog" />
}
