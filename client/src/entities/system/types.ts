export interface ViewPort {
  width: number
  height: number
}

export enum ViewPortWidthType {
  Desktop = 1200,
  Tablet = 769,
  Phone = 576
}
