export const getNextReqInterval = (timestamp: number) => (timestamp - Number(Date.now())) / 1000
