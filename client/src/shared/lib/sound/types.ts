export type Sound = 'message-delivered' | 'calling' | 'busy' | 'connection' | 'ring'
