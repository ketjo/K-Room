export enum ImageResolutions {
  png = 'image/png',
  jpeg = 'image/jpeg',
  jpg = 'image/jpg'
}
