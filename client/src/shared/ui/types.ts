export type ShapeModifier = 'round' | 'square' | 'default' | 'circle' | 'round'
export type ColorModifier = 'accent' | 'success' | 'error' | 'warn' | 'default' | 'white' | 'black'
export type SizeModifier = 'extra-small' | 'small' | 'middle' | 'large' | 'extra-large'
