export const welcome = (): string => `<div class="welcome">
  <h1>Welcome to K-Room!</h1>
  <p>We are excited to introduce you to the beta version of our web app, where you can seamlessly communicate with your friends and loved ones. Connect through private chats, exchange text messages, share photos, and engage in both regular and video calls.</p>
  <p>Create your own group chats to bring together friends, colleagues, or family members, allowing for seamless communication and information exchange within the group.</p>
  <p>Enjoy the ability to make high-quality video calls with your loved ones, no matter where they are located. Share life's brightest moments with friends by sending and receiving photos right within the chat.</p>
  <p>As we continue to develop the website, expect even more features and improvements to enhance your experience.</p>
</div>`
