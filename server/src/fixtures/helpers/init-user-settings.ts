import { UserSettings } from '../../@types'

export const initUserSettings: UserSettings = {
  selectedContentElement: 'contacts',
  selectedAdminPanelModelTab: 'users',
  currentInfoId: '1',
  selectedChatRoomId: '',
  theme: 'dark',
  soundOn: true,
  showTooltips: false,
  ableToShowNotification: true,
  showWallpaper: true
}
