export const initUserCodes = {
  passwordRecovery: {
    query: {
      value: '',
      expiresIn: ''
    },
    email: null,
    sms: null
  },
  nextRequestPossibleAt: null
}
