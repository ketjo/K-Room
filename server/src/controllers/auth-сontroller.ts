import { Request, Response } from 'express'
import { v4 as uuidv4 } from 'uuid'
import { initUserSettings, initUserCodes } from '../fixtures'
import { authValidator } from '../middlewares'
import { UserModel } from '../models'
import { updateTokens, getInfo, sendEmailConfirmationLink } from '../services'
import { ServerNotificationMessage, Status, UserCredential } from '../@types'
import { throwError } from '../utils'

const bcrypt = require('bcryptjs')

class AuthController {
  async updateTokensPair(req: Request, res: Response) {
    const userId = req.app.locals.id
    await updateTokens(userId, res)
    res.json({ message: ServerNotificationMessage.TokensPairUpdated, silent: true })
  }

  async registration(req: Request, res: Response) {
    try {
      authValidator(req, res)
      const { username, email, password } = req.body
      const userNameCandidate = await UserModel.findOne({ username })

      if (userNameCandidate) {
        return throwError(Status.BadRequest, res, ServerNotificationMessage.UserWithCurrentNameAlreadyExist)
      }

      const emailCandidate = await UserModel.findOne({ email })

      if (emailCandidate) {
        return throwError(Status.BadRequest, res, ServerNotificationMessage.UserWithCurrentEmailAlreadyExist)
      }

      const hashedPassword = await bcrypt.hash(password, 6)

      if (!hashedPassword) return throwError(Status.BadRequest, res, ServerNotificationMessage.FailedPassHash)
      const welcomeInfoItem = getInfo('1')
      const user = new UserModel({
        username,
        email,
        password: hashedPassword,
        socketId: '',
        settings: initUserSettings,
        codes: initUserCodes,
        infoItems: [welcomeInfoItem],
        role: 'user'
      })

      await user.save()

      const confirmEmailData = await sendEmailConfirmationLink(req.body.email)
      if (!confirmEmailData) {
        return throwError(Status.Unreachable, res, ServerNotificationMessage.FailedSendConfirmationLink)
      }

      return res.json(confirmEmailData)
    } catch {
      throwError(Status.BadRequest, res, ServerNotificationMessage.FailedRegistration)
    }
  }

  async sendConfirmationLink(req: Request, res: Response) {
    const { email } = req.body
    try {
      const confirmEmailData = await sendEmailConfirmationLink(email)
      return res.json(confirmEmailData)
    } catch {
      throwError(Status.Unreachable, res, ServerNotificationMessage.FailedSendConfirmEmail)
    }
  }

  async confirmEmail(req: Request, res: Response) {
    try {
      const userId = req.body.userId
      const user = await UserModel.findOneAndUpdate({ _id: userId }, { confirmed: true }, { new: true })
      if (!user) return
      return res.json({
        userData: { username: user.username, email: user.email, id: user._id, avatar: user.avatarPath },
        message: ServerNotificationMessage.EmailConfirmed
      })
    } catch {
      throwError(Status.BadRequest, res, ServerNotificationMessage.FailedEmailConfirm)
    }
  }

  async login(req: Request, res: Response) {
    try {
      const { email, password } = req.body
      const user = await UserModel.findOne({ email })
      if (!user) return throwError(Status.BadRequest, res, ServerNotificationMessage.UserNotFound)
      if (!user.confirmed) return throwError(Status.BadRequest, res, ServerNotificationMessage.EmailNotConfirm)

      const validPassword = bcrypt.compareSync(password, user.password)

      if (!validPassword) return throwError(Status.BadRequest, res, ServerNotificationMessage.WrongPass)
      await updateTokens(user._id, res)
      return res.json({
        userData: {
          role: user.role,
          username: user.username,
          email,
          id: user._id,
          avatarPath: user.avatarPath,
          infoItems: user.infoItems
        },
        settings: user.settings,
        message: ServerNotificationMessage.LoginSuccess,
        silent: true
      })
    } catch {
      throwError(Status.BadRequest, res, ServerNotificationMessage.FailedLogin)
    }
  }

  async signInWithProvider(req: Request, res: Response) {
    try {
      const { username, email, avatarPath, providerName }: UserCredential = req.body
      let user = await UserModel.findOne({ email })
      if (!user) {
        const hashedPassword = await bcrypt.hash(uuidv4(), 6)
        user = new UserModel({
          username,
          role: 'user',
          email,
          avatarPath,
          providerName,
          password: hashedPassword,
          socketId: '',
          confirmed: true,
          settings: initUserSettings,
          codes: initUserCodes
        })
        await user.save()
      }

      await updateTokens(user._id, res)

      return res.json({
        userData: {
          username: user.username ?? username,
          email,
          id: user?._id,
          avatarPath: user.avatarPath ?? avatarPath,
          role: user.role
        },
        settings: user.settings,
        message: ServerNotificationMessage.LoginAndRegister
      })
    } catch {
      throwError(Status.BadRequest, res, ServerNotificationMessage.FailedLogin)
    }
  }
}

export const controller = new AuthController()
