export interface JWTDecoded {
  id: string
  iat: number
  exp: number
}
