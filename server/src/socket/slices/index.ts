import { contactsSlice } from './contacts-slice'
import { commonSlice } from './common-slice'
import { chatRoomSlice } from './chat-room-slice'
import { messageSlice } from './message-slice'
import { callSlice } from './call-slice'

export const slices = { contactsSlice, commonSlice, chatRoomSlice, messageSlice, callSlice }
